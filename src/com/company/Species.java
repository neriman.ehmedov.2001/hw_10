package com.company;

import java.util.*;

public enum Species {
    Dog,
    DomesticCat,
    RoboCat,
    Fish,
    UNKNOWN;
}
