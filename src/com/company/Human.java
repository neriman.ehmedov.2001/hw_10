package com.company;

import java.time.*;
import java.time.chrono.ChronoLocalDate;
import java.time.chrono.ChronoPeriod;
import java.time.format.DateTimeFormatter;
import java.util.*;

public abstract class Human {
    // field
    protected String name, surname;
    protected int iq;
    protected LocalDate birthDay;
    protected Family family;
    protected Map<String, String> schedule = new HashMap<String, String>();
    protected DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("dd/MM/yyyy");


    // constructors
    public Human() {
    }
    public Human(String name, String surname, int y, int m, int d) {
        this.name = name;
        this.surname = surname;
        this.birthDay = LocalDate.of(y, m, d);
    }
    public Human(String name, String surname, int y, int m, int d, int iq) {
        this.name = name;
        this.surname = surname;
        this.birthDay = LocalDate.of(y, m, d);
        this.iq = iq;
    }

    // methods
    public void describePet() {
        System.out.print("I have a " + family.getPet().getSpecies() +
                ", he is " + family.getPet().getAge() + " years old, he is ");
        System.out.println((family.getPet().getTrickLevel() > 50) ? "very sly." : "almost not sly.");
    }
    protected static void calculateChronoPeriod(ChronoLocalDate startDate, ChronoLocalDate endDate) {
        ChronoPeriod chronoPeriod = ChronoPeriod.between(startDate, endDate);
        String res = chronoPeriod.toString();
        for(int i = 1; i < res.length()-2; i++){
            if(res.charAt(i) == 'Y' || res.charAt(i) == 'M')
                System.out.print('/');
            else
                System.out.print(res.charAt(i));
        }
    }
    public void describeAge() {
        LocalDate date = LocalDate.now();
        ChronoLocalDate startDate = birthDay;
        ChronoLocalDate endDate = date;
        calculateChronoPeriod(startDate, endDate);
        System.out.println();
    }

    // Override methods
    @Override
    public String toString() {
        return "Human{" +
                "name = '" + name + '\'' +
                ", surname = '" + surname + '\'' +
                ", year = " + getBirthDay().format(myFormatObj) +
                ", iq = " + iq +
                ", schedule = " + getSchedule() + "}";
    }

    // setters
    public void setName(String name) {
        this.name = name;
    }
    public void setSurname(String surname) {
        this.surname = surname;
    }
    public void setBirthDay(int y, int m, int d) {
        this.birthDay = LocalDate.of(y, m, d);
    }
    public void setIq(int iq) {
        this.iq = iq;
    }
    public void setFamily(Family family) {
        this.family = family;
    }
    public void setSchedule(String day, String doing){
        schedule.put(day, doing);
    }

    // getters
    public String getName() {
        return name;
    }
    public String getSurname() {
        return surname;
    }
    public LocalDate getBirthDay() {
        return birthDay;
    }
    public int getIq() {
        return iq;
    }
    public Family getFamily() {
        return family;
    }
    public String getSchedule() {
        String res = "";
        for(Map.Entry<String, String> i : schedule.entrySet())
            res += ("\n" + i.getKey() + " - " + i.getValue());
        return res;
    }
}

final class Man extends Human {
    // constructors
    public Man() {
    }
    public Man(String name, String surname, int y, int m, int d) {
        this.name = name;
        this.surname = surname;
        this.birthDay = LocalDate.of(y, m, d);
    }
    public Man(String name, String surname, int y, int m, int d, int iq) {
        this.name = name;
        this.surname = surname;
        this.birthDay = LocalDate.of(y, m, d);
        this.iq = iq;
    }

    // methods
    public void repairCar() {
        System.out.println("It's time to repair car.");
    }
    public void greetPet() {
        System.out.println("Hello, " + family.getPet().getNickname() + '.');
    }
}
final class Woman extends Human {
    // constructors
    public Woman() {
    }
    public Woman(String name, String surname, int y, int m, int d) {
        this.name = name;
        this.surname = surname;
        this.birthDay = LocalDate.of(y, m, d);
    }
    public Woman(String name, String surname, int y, int m, int d, int iq) {
        this.name = name;
        this.surname = surname;
        this.birthDay = LocalDate.of(y, m, d);
        this.iq = iq;
    }

    // methods
    public void makeUp() {
        System.out.println("It's time to go to the beautician.");
    }
    public void greetPet() {
        System.out.println("Hello, my sweeties " + family.getPet().getNickname() + '.');
    }
}
